RHL 2019, static copy of the website
====================================

This is a static copy of the website of the 2019 verson of Rencontres
Hivernales du Libre.



Build
-----

With Podman:
```sh
podman build -t 2019.hivernal.es:latest .
```


With Docker:
```sh
docker build -t 2019.hivernal.es:latest .
```


RUN
---

With Podman:
```sh
podman run 2019.hivernal.es:latest
```

With Docker:
```sh
docker run 2019.hivernal.es:latest
```


Author
------

Sébastien Gendre <seb@k-7.ch>
